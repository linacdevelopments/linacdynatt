# Linac DynamicAttributes loader

GUI to save and load Tango DynamicAttributes for Alba PLC DS.

## Usage

Go to any control room machine and execute:

```bash
operator@crpcNN$ ctlidynamicattributes
# NN = Machine  number
```

A GUI will appear:

![alt text][img1]


[img1]:./Images/image_1.png "GUI image"

To run the GUI you will have to select one option and press start. 

Options are:
 - Save Dynamic Attributes
 - Load DynamicAttributes (1 PLC)
 - Load DynamicAttributes (All PLC)

### 1. Save actual DynamicAttributes
When you run this option, program will:
 
 1. Read DynamicAttributes from each Linac PLC device server. 
 2. If not exist: Create day folder inside: `/data/LINAC/DynamicAttributes
    /PLC_Backup/`
 3. Create hour folder inside `/data/LINAC/DynamicAttributes/PLC_Backup
    /day_folder/`
 4. Save each PLC DynamicAttributes in a separated file `li_ct_plcN.linac` 
    (N= PLC number)

### 2. Load DynamicAttributes (1 PLC)
When you run this option, program will:
 1. Save ALL actual PLC DynamicAttributes (See point 1)
 2. Open dialog to select a `.linac` file.
 3. Write DynamicAttribute in PLC.

#### File format
If you want to create a file to load a DynamicAttributes property, first line 
must be `#device/to/linac_plc` and then DynamicAttributes property.

When we load a file, we don't ask for a PLC device server because program will
expect that fist line is a comment with linac device server.

*Note: You can load a simple text file: `your_file.txt`.*
- Example:

```python
#li/ct/plc1
Attr('A0_OP', tango.DevFloat,
     maxValue=950)

## section of attribute definition
# define a readonly float: name and tango type is mandatory always
Attr('float_scalar_ro', tango.DevFloat,  # cloning Vdrain
    read_addr=166,
    label='float scalar read only',
    format='%3.1f',
    events={THRESHOLD: 0.1},
    # setpoint='float_scalar_rw'  # not available feature within this property
    )

# define a read/write float attribute: only define the write_addr because the 
# read_addr is calculated based in the blocks sizes
Attr('float_scalar_rw', tango.DevFloat,  # cloning Vdrain_setpoint
    write_addr=82,
    label='float scalar read write',
    format='%3.1f',
    events={THRESHOLD: 0.1})

Attr('short_scalar_ro', tango.DevShort,  # TB_KA1_Delay converted to RO
    read_addr=148,
    label='short scalar read only',
     minValue=1, maxValue=56, unit='ns', events={}, format="%2d")

Attr('short_scalar_rw', tango.DevShort,  # cloning TB_KA1_Delay
     write_addr=64,  # RW
     label='short scalar read write',
     minValue=1, maxValue=56, unit='ns', events={}, format="%2d")

Attr('uchar_scalar_ro', tango.DevUChar,  # cloning Gun_HV_ST
     read_addr=74,
     label='uchar scalar read only',
     desc='high voltage PS status: 0:undefined, 1:off, 2:locked, 3: fault, 4:ready, 5: unlocked',
     meanings={0: 'undefined', 1: 'off', 2: 'locked', 3: 'fault', 
               4: 'ready', 5: 'unlocked'},
     qualities={ALARM: [0, 3, 5], WARNING: [1, 2, 5]},
     events={})

AttrBit('boolean_scalar_ro',  # cloning A0_ST
    read_addr=68, read_bit=4,
    label='boolean scalar read only',
    meanings={0: 'off', 1: 'ready'},
    qualities={WARNING: [False]},
    events={})

AttrBit('boolean_scalar_rw',  # cloning TB_MBM
    write_addr=78, write_bit=0, 
    label='boolean scalar read write',
    desc='timer multi bunch mode enabled; False:SBM, True:MBM',
    meanings={0: 'SBM', 1: 'MBM'},
    # qualities={WARNING: [False]},
    events={})

## section of existing attributes modification:
# this is how one may redefine an already existing attribute, for example to 
# increase its event emittion sensitivity
Attr('Vdrain', tango.DevFloat, 
    # read_addr=166,
    # label='Voltage drain',
    # format='%6.2f', minValue=5, maxValue=46, unit='V',
    events={THRESHOLD: 0.01},
    # setpoint='Vdrain_setpoint'
    )

# this is how one may modify the bounds of a write attribute or the format 
# it showns
Attr('Vdrain_setpoint', tango.DevFloat,
    # write_addr=82,  # (readsize-writesize)+82=(170-86)+82=166
    # label='Voltage drain setpoint',
    format='%6.3f', minValue=-1, maxValue=76, unit='V',
    events={THRESHOLD: 0.5})

# modify the warning quality of an existing attribute
Attr('SF6_P2', tango.DevFloat, 
    # read_addr=52,  # RO
    # label='SF6 pressure 2',
    # format='%4.2f', minValue=-1, maxValue=5, unit='bar',
    # events={THRESHOLD: 0.001},
    qualities={WARNING: {ABSOLUTE: {BELOW: 2.9, ABOVE: 3.15}}})
```

### 3. Load DynamicAttributes (All PLC)
Option to recover ALL PLC from autosaved files. Load file dialog will be launched
in  `/data/LINAC/DynamicAttributes/PLC_Backup/`

Select a folder and ALL PLC device servers will be recovered with DynamicAttributes 
files found in this folder.

## For Controls Section
There is a feature since version 0.2.14 that update a internal git repository.


### Controls folder
You can find a git repo on `/data/LINAC/DynamicAttributes/.control/`. Everytime
`SaveDynAtt()` is called, we copy last saved files to this folder and commit.

If you want to see evolution or compare, you can use regular git tools to 
compare, diff, see commit evolution, etc.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to 
discuss what you would like to change.


## License
[GNU GENERAL PUBLIC LICENSE](LICENSE)


