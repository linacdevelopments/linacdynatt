#!/usr/bin/env python

from setuptools import setup, find_packages

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  LinacDynAtt
#  Copyright (C) 2021  Controls
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Emilio Morales"
__copyright__ = "Copyright 2021, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

# The version is updated automatically with bumpversion
# Do not update manually
__version = '0.3.1'


setup(
    name="LinacDynAtt",
    license=__license__,
    version=__version,
    author="Emilio Morales",
    author_email="emorales@cells.es",
    maintainer='ct4',
    maintainer_email='ct4@cells.es',
    packages=find_packages(),
    entry_points={
        'console_scripts': [],
        'gui_scripts': [
            'ctlidynamicattributes = LinacDynAtt.ctlidynamicattributes:main',
            ]
        },
    options={
        'build_scripts': {
                'executable': '/usr/bin/env python',
                    },
        },
    keywords='GUI',
    include_package_data=True,
    description="DynamicAttributes Loader",
    long_description="GUI to save and load Dynamic attributes for Linac PLC",
    requires=['setuptools (>=1.1)'],
    classifiers=['Development Status :: 2 - Pre-Alpha',
                 'Intended Audience :: Science/Research',
                 'License :: OSI Approved :: '
                 'GNU General Public License v3 or later (GPLv3+)',
                 'Programming Language :: Python',
                 'Topic :: Scientific/Engineering :: '
                 ''],
    url="https://git.cells.es/controls/LinacDynAttLoader",
)
